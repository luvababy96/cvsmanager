package com.lsw.cvsmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Info {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 2)
    private String gender;
    @Column(nullable = false, length = 50)
    private String item;
    @Column(nullable = false)
    private LocalDate date;
    @Column(nullable = false, length = 3)
    private Integer age;


}
