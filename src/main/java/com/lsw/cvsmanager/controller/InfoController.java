package com.lsw.cvsmanager.controller;

import com.lsw.cvsmanager.model.InfoRequest;
import com.lsw.cvsmanager.model.PeopleList;
import com.lsw.cvsmanager.service.InfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/info")
public class InfoController {

    private final InfoService infoService;
    @PostMapping("/data")
    public String setInfo(@RequestBody InfoRequest request) {
        infoService.setInfo(request.getGender(),request.getItem(),request.getDate(), request.getAge());
        return "OK";
    }
    @GetMapping("/infoes")
    public List<PeopleList> getInfoes() {
        List<PeopleList> result = infoService.getInfoes();
        return result;
    }

    @GetMapping("/allInfo")
    public List<InfoRequest> getAllInfo() {
        List<InfoRequest> result = infoService.getAllInfo();
        return result;
    }

}
