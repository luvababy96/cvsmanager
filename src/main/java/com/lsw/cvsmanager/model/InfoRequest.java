package com.lsw.cvsmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class InfoRequest {

    private String gender;

    private String item;

    private LocalDate date;

    private Integer age;

}
