package com.lsw.cvsmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CvsManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CvsManagerApplication.class, args);
    }

}
