package com.lsw.cvsmanager.service;

import com.lsw.cvsmanager.entity.Info;
import com.lsw.cvsmanager.model.InfoRequest;
import com.lsw.cvsmanager.model.PeopleList;
import com.lsw.cvsmanager.repository.InfoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class InfoService {

    private final InfoRepository infoRepository;

    public void setInfo(String gender, String item, LocalDate date,Integer age){
        Info addData = new Info();
        addData.setGender(gender);
        addData.setItem(item);
        addData.setDate(date);
        addData.setAge(age);

        infoRepository.save(addData);
    }

    public List<PeopleList> getInfoes() {
        List<PeopleList> result = new LinkedList<>();

        List<Info> originData = infoRepository.findAll();

        for (Info item : originData) {
            PeopleList additem = new PeopleList();
            additem.setGender(item.getGender());
            additem.setAge(item.getAge());

            result.add(additem);
        }
        return result;
    }

    public List<InfoRequest> getAllInfo() {
        List<InfoRequest> result = new LinkedList<>();

        List<Info> originData = infoRepository.findAll();

        for (Info item : originData) {
            InfoRequest additem = new InfoRequest();
            additem.setGender(item.getGender());
            additem.setAge(item.getAge());
            additem.setDate(item.getDate());
            additem.setItem(item.getItem());

            result.add(additem);
        }
        return result;
    }
}
