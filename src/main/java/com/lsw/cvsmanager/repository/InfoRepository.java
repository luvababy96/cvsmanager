package com.lsw.cvsmanager.repository;

import com.lsw.cvsmanager.entity.Info;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InfoRepository extends JpaRepository<Info, Long> {
}
